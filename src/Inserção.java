
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Inserção {
    public void inserir(String nome, String idade, String imperium, String família){
        try {
            Connection c = Conexao.obterConexao();
            PreparedStatement ps = c.prepareStatement("INSERT INTO SC_SPQR.CÔNSUL(NOME, IDADE, IMPERIUM, FAMÍLIA) VALUES(?,?)");
            ps.setString(1,nome);
            ps.setString(2,idade);
            ps.setString(3,imperium);
            ps.setString(4,família);
            ps.executeQuery();
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(Inserção.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
